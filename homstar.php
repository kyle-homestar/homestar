<?php
/**
* Plugin Name: Homstar Listing
* Plugin URI: http://homstar.com.au
* Description: This is a plug-in to view listings
* Version: 4.1.0
* Author: Homstar
* Author URI: http://homstar.com.au
* Bitbucket Plugin URI: https://bitbucket.org/kyle-homestar/homestar
**/

function homstar_files() {
    wp_register_style('homstar-styles', plugins_url('style.css',__FILE__ ));
}

add_action( 'admin_init','homstar_files');

add_filter('the_title', 'wpshout_filter_example');
function wpshout_filter_example($title) {
	return 'Title: '.$title;
}

